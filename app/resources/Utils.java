package resources;

import java.util.Calendar;
import java.util.Date;

public class Utils {
	
	/**
	 * This is a specific period that specify a valid cycle to vote 
	 * Cycle: beteewen 00:00 and 11:00 at the same day
	 */
	public static Calendar getVoteCycleInit() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.HOUR_OF_DAY, 0); //TODO: hard code?!
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal;
	}
	public static Calendar getVoteCycleEnd() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.HOUR_OF_DAY, 11); //TODO: hard code?!
		cal.set(Calendar.MINUTE, 45);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal;
	}
	public static Calendar getElectionCycleInit() {
		Calendar cal = getVoteCycleEnd();
		cal.add(Calendar.MINUTE, 1);
		return cal;
	}
	public static Calendar getElectionCycleEnd() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.HOUR_OF_DAY, 23); //TODO: hard code?!
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 0);
		return cal;
	}
	public static Calendar getLastWeekDate() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DAY_OF_MONTH, -7);
		System.out.println(cal.getTime());
		return cal;
	}
	public static Boolean isValidElectionCycle() {
		Calendar calNow = Calendar.getInstance();
		calNow.setTime(new Date());
		
		Calendar calInit = getVoteCycleInit();
		Calendar calEnd = getVoteCycleEnd();
		
		Boolean isValid = calNow.after(calInit) && calNow.before(calEnd);
		
		return isValid;
	}
}
