package models;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import play.db.ebean.Model;

@Entity
@Table(name="space_neighbors")
public class SpaceNeighbors extends Model implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private SpaceNeighborId spaceNeighbor;
	
	public SpaceNeighbors() {
		super();
	}
	public SpaceNeighborId getSpaceNeighbor() {
		return spaceNeighbor;
	}
	public void setSpaceNeighbor(SpaceNeighborId spaceNeighbor) {
		this.spaceNeighbor = spaceNeighbor;
	}
	public Long getSpaceId() {
		return spaceNeighbor.getSpaceId();
	}
	public Long getNeighborId() {
		return spaceNeighbor.getNeighborId();
	}
	
}


