package models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;


@Entity
@Embeddable
public class SpaceNeighborId implements Serializable {

	private static final long serialVersionUID = 2843059533271810563L;
	
	@Column(name="space_id")
	private Long spaceId;
	@Column(name="neighbor_id")
	private Long neighborId;
	
	public Long getSpaceId() {
		return spaceId;
	}
	public void setSpaceId(Long spaceId) {
		this.spaceId = spaceId;
	}
	public Long getNeighborId() {
		return neighborId;
	}
	public void setNeighborId(Long neighborId) {
		this.neighborId = neighborId;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((neighborId == null) ? 0 : neighborId.hashCode());
		result = prime * result + ((spaceId == null) ? 0 : spaceId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SpaceNeighborId other = (SpaceNeighborId) obj;
		if (neighborId == null) {
			if (other.neighborId != null)
				return false;
		} else if (!neighborId.equals(other.neighborId))
			return false;
		if (spaceId == null) {
			if (other.spaceId != null)
				return false;
		} else if (!spaceId.equals(other.spaceId))
			return false;
		return true;
	}
	
	
}

