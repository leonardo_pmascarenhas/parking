package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name = "space")
public class Space extends Model implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="ID")
	private Long id;
	
    @ManyToMany(cascade=CascadeType.ALL)
    @JoinTable(
    		name="space_neighbors",
    		joinColumns={@JoinColumn(name="space_id", referencedColumnName="ID")},
    		inverseJoinColumns={@JoinColumn(name="neighbor_id", referencedColumnName="ID")})
    
//    @JoinTable(
//		name = "space_neighbors", 
//		joinColumns = { @JoinColumn(name = "space_id") }, 
//		inverseJoinColumns = { @JoinColumn(name = "neighbor_id")
//	})
    private List<Space> neighbors;
    
    private Boolean used;
	
	public Space() {
		super();
		
		this.setUsed(false);
		this.setNeighbors(new ArrayList<Space>());
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Space> getNeighbors() {
		if (this.neighbors == null) {
			return null;
		}
		
		return neighbors;
	}

	public void setNeighbors(List<Space> neighbors) {
		this.neighbors = neighbors;
	}
	
	public Boolean getUsed() {
		return used;
	}

	public void setUsed(Boolean used) {
		this.used = used;
	}
	
	public int countNeighbors() {
		return this.getNeighbors().size();
	}
	
	/*
	 * Static Resources
	 */

	public static void createMany(int num) {
		int i;
		for (i=0; i < num; i++) {
			Space space = new Space();
			space.save();
		}
	}
	
	public static Model.Finder<String, Space> find = new Model.Finder<String, Space>(
			String.class, Space.class);

	public static List<Space> findAll() {
		return find.all();
	}
	
	public static Space findById(Long id) {
		return find.byId(String.valueOf(id));
	}
	
	public static List<Space> findNeighborsBySpaceId(Long id) {
		Space space = find.byId(String.valueOf(id));
		
		if (space == null) {
			return new ArrayList<Space>();
		}
		
		return space.getNeighbors();
	}

}
