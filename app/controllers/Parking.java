package controllers;

import java.util.ArrayList;
import java.util.List;

import models.Space;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

public class Parking extends Controller {
	
	final static Form<Space> spaceForm = Form.form(Space.class);
	final static Form<Object> genericForm = Form.form(Object.class);
	
	public static Result manager() {
		List<Space> spaces = Space.findAll();
		return ok(views.html.parking.manager.render(spaces));
	}
	
	public static Result create() {
		return ok(views.html.parking.create.render(genericForm));
	}
	public static Result createMany() {
		Form<Object> form = genericForm.bindFromRequest();
		
		int num = 0;
		try {
			num = Integer.parseInt(form.data().get("num"));
		} catch (Exception e) {
			System.out.println("[Error] Trying to convert an integer value.\n" + e);
			return badRequest(views.html.parking.create.render(form));
		}
		
		Space.createMany(num);

		List<Space> spaces = Space.findAll();
		return ok(views.html.parking.manager.render(spaces));
	}
	
	public static Result edit(Long id) {
		List<Space> neighbors = Space.findNeighborsBySpaceId(id);
		return ok(views.html.parking.edit.render(genericForm, neighbors, id));
	}
	
	public static Result update(Long id) {
		Form<Space> form = spaceForm.bindFromRequest();

		Long neighborId;
		try {
			neighborId = Long.valueOf(form.data().get("neighborId"));
		} catch (Exception e) {
			System.out.println("[Error] Trying to convert an integer value.\n" + e);
			List<Space> neighbors = Space.findNeighborsBySpaceId(id);
			return badRequest(views.html.parking.edit.render(form, neighbors, id));
		}
		
		if (id == neighborId) {
			List<Space> neighbors = Space.findNeighborsBySpaceId(id);

			flash("error", "Sorry, but the neighbor " + neighborId + " is the same space. This is not accepted.");
			return redirect(routes.Parking.edit(id));
		}

		Space neighbor = Space.findById(neighborId);
		if (neighbor == null) {
			List<Space> neighbors = Space.findNeighborsBySpaceId(id);

			flash("error", "Sorry, but the space " + neighborId + " do not exist.");
			return redirect(routes.Parking.edit(id));
		}

		Space space = Space.findById(id);
		space.getNeighbors().add(neighbor);
		space.update();

		flash("success", "success");

		return redirect(routes.Parking.edit(id));
	}

	public static Result delete(Long id) {
		Space space = Space.findById(id);

		if (space == null) {
			flash("error", "error");
			return found(routes.Parking.manager());
		}

		space.delete();
		flash("success", "success");

		return found(routes.Parking.manager());
	}
	
	public static Result deleteNeighbor(Long spaceId, Long neighborId) {
		System.out.println("neighbor: " + neighborId);

		Space editedSpace = Space.findById(spaceId);
		Space deletedNeighbor = Space.findById(neighborId);
		
		editedSpace.getNeighbors().remove(deletedNeighbor);
		editedSpace.save();
		
		//we don't need an answer here, just 200
		return ok();
	}
}
