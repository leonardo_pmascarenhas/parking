# Parking Manager

## Objective
The objective is provide a way to manage a parking lot.

## Instructions to run

### Create a mySQL database named "parking" or change the configuration in:
```
conf/application.conf
```

### Install play framework
[Download and instructions](https://www.playframework.com/download)

### Use the command line
Run the project from its directory:

*The first time this takes a while, is when the Apache Ivy (via sbt) implement managed dependencies*
```
activator run
```

Enter the interactive cli (in project directory):
```
activator
```

**if you have some issues with dependencies or if the sbt cache is corrupted, clean and try again**
```
activator clean
```